<?php

ob_start();

require __DIR__ . "/vendor/autoload.php";

/** Bootstrap */

use CoffeeCode\Router\Router;
use Source\Core\Session;

$session = new Session();
$route = new Router(url(), ":");
$route->namespace("Source\App");

$route->group(null);
$route->get("/", "Web:home");
$route->get("/quem-somos", "Web:aboutUs");
$route->get("/contato", "Web:contact");
$route->get("/trabalhe-conosco", "Web:jobs");

$route->post("/mensagem","Web:mensagem");


/** Rotas de Erro */
$route->group("/ops");
$route->get("/{errcode}", "Web:error");

/** ROUTE */
$route->dispatch();

/** ERROR Redirect */
if ($route->error()) {
  $route->redirect("/ops/{$route->error()}");
}

ob_end_flush();
