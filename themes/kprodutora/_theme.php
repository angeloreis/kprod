<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <meta charset="UTF-8">
  <meta name="mit" content="2020-06-25T21:46:29-03:00+42687">
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <?= $head; ?>

  <link rel="icon" type="image/png" href="<?= theme("/assets/images/favicon.png"); ?>" />
  <link rel="stylesheet" href="<?= theme("/assets/style.css"); ?>" />
</head>

<body>

<div class="ajax_load">
    <div class="ajax_load_box">
        <div class="ajax_load_box_circle"></div>
        <p class="ajax_load_box_title">Aguarde, carregando...</p>
    </div>
</div>

  <!-- Menu -->
  <header class="layout-cabecalho layout-cabecalho--home">
    <div class="container">
      <nav class="navegacao">
        <a href="<?= url(); ?>">
          <img src="<?= theme("/assets/images/logo-site.png"); ?>" width="20%" alt="<?= CONF_SITE_NAME . ' - ' . CONF_SITE_DESC ?>">
        </a>

        <div class="navegacao__menu">
          <button class="botao-chaveador js-btn-chaveador"></button>

          <ul class="menu js-menu-mobile">
            <li class="menu__item"><a href="<?= url(); ?>">Home</a></li>
            <li class="menu__item"><a href="<?= url("/quem-somos"); ?>">Quem Somos</a></li>
            <li class="menu__item"><a href="<?= url("/#portfolio"); ?>">Portfólio</a></li>
            <!--li class="menu__item"><a href="<?= url("/trabalhe-conosco"); ?>">Trabalhe Conosco</a></li-->
            <li class="menu__item"><a href="<?= url("/#contato"); ?>">Contato</a></li>
            <li class="menu__item menu__item--botao"><a href="https://webmail.kprodutoradevideo.com.br" target="_blank" class="botao botao--login">Webmail</a></li>
          </ul>

        </div>
      </nav>
    </div>
  </header>
  <!--CONTENT-->
  <main class="main_content">
    <?= $v->section("content"); ?>
  </main>

  <footer class="layout-rodape">
    <div class="container">
      <p> K Produtora de Vídeo - Todos os direitos Reservados @ 2020 - Desenvolvido por <a href="https://angeloreis.github.io" target="_blank" class="link"> Angelo Reis</a></p>
    </div>
  </footer>

  <script src="<?= theme("/assets/scripts.js"); ?>"></script>
  <script src="https://player.vimeo.com/api/player.js"></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=<?= $gAnalytcsCode; ?>"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', '<?= $gAnalytcsCode; ?>');
  </script>

  <?= $v->section("scripts"); ?>

</body>

</html>
