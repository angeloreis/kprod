<?php $v->layout("_theme"); ?>

<!-- Início da Chamada principal -->
<section class="layout-chamada">
  <div class="container">

    <div class="chamada">
      <h1 class="chamada__subtitulo">VOCÊ IMAGINA,</h1>
      <p class="chamada__titulo">NÓS REALIZAMOS!</p>
      <div class="chamada__acao">
        <a href="<?= url("/contato"); ?>" class="botao  botao--chamada">
          Entre em contato
        </a>
      </div>
    </div>

  </div>
</section>
<!-- Fim da Chamada principal -->

<section class="layout-servicos">
  <div class="container servicos">
    <p class="chamada__centro">Serviços</p>
    <div class="row">
      <div class="col-sm-4">
        <article class="servico__box">
          <img src="<?= theme("/assets/images/photography.png"); ?>">
          <h2>Vídeos</h2>
          <span>
            <ul class="plano__carateristicas">
              <li>Alta Resolução</li>
              <li>Captação de aúdio limpa</li>
              <li>Cores vivas</li>
            </ul>
          </span>
        </article>
      </div>
      <div class="col-sm-4">
        <article class="servico__box">
          <img src="<?= theme("/assets/images/drone.png"); ?>">
          <h2>Drone</h2>
          <ul class="plano__carateristicas">
            <li>Ângulos diferenciados</li>
            <li>Equipamento registrado</li>
            <li>Alta Resolução</li>
          </ul>

        </article>
      </div>
      <div class="col-sm-4 ultima-coluna">
        <article class="servico__box">
          <img src="<?= theme("/assets/images/broadcast.png"); ?>">
          <h2>Lives</h2>
          <ul class="plano__carateristicas">
            <li>Assessoria técnica</li>
            <li>Criação de Padrão visual</li>
            <li>Internet rápida e com redundância</li>
          </ul>

        </article>
      </div>
    </div>
  </div>
</section>

<section id="Portfolio" class="layout-portifolio">
  <div class="container servicos">
    <p class="chamada__centro">Portfólio</p>
    <div class="col-sm-12 chamada__centro">
      <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/62418632?color=6b1010&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:30px;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
      </div>
    </div>
</section>

<section id="Localizacao" class="layout-localizacao servicos">
  <div class="container servicos">
    <p class="chamada__centro">Localização</p>
    <div class="col-sm-12 chamada__centro">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d997.1388578064107!2d-48.468496770825595!3d-1.4416343999342787!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x92a48c17447fe3d1%3A0x7c1f8e3dfbe55079!2sVila%20Cardoso%2C%204498%20-%20Marco%2C%20Bel%C3%A9m%20-%20PA%2C%2066023-015!5e0!3m2!1spt-BR!2sbr!4v1593378837616!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0; text-align:center; border-radius: 30px;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
  </div>
</section>