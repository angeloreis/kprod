<?php $v->layout("_theme"); ?>
<div class="layout-quem-somos">
  <div class="container">
    <div class="row">
      <div class="quem-somos col-sm-12">
        <h1>Quem somos</h1>

        <p>A KPRODUTORA é uma empresa que pensa além de um simples vídeo. Trazemos agilidade e qualidade para você se conectar melhor com seu público.</p>
        <p>Com uma equipe especializada, Trabalhamos com produção de vídeos institucionais, promocionais, perfis políticos, eventos, documentários, comerciais, programas de TV (abertura e/ou corporativa) e para mídias digitais (websites e redes sociais).</p>
        <p>Com profissionalismo e dedicação, levamos ao mercado produções diferenciadas e com os melhores equipamentos do mercado.</p>
        <p>Atuamos em todas as etapas da produção de um vídeo. Captação, edição e finalização em diversos formatos.</p>
        <p>Estamos no mercado para fazer o diferencial, com responsabilidade e inovação. Venha nos conhecer !!!</p>
      </div>
    </div>
  </div>
</div>
