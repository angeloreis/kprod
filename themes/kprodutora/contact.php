<?php $v->layout("_theme"); ?>

<div class="layout-contato">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <h4 class="titulo-formulario">Entre em contato conosco</h4>
                <div class="grupo-entrada">
                    <form action="<?= url("/mensagem"); ?>" method="post" enctype="multipart/form-data">
                        <div class="ajax_response"><?= flash(); ?></div>
                        <?= csrf_input(); ?>
                        <input type="text" name="first_name" class="campo" placeholder="Nome completo Ou Razao Social:" required/>
                        <input type="email" name="email" class="campo" placeholder="Melhor e-mail:" required/>
                        <input type="text" name="subject" class="campo" placeholder="Assunto:"/>
                        <input type="text" name="content" class="campo" placeholder="Mensagem:"/>
                        <input type="password" name="password" class="campo" placeholder="Senha de acesso:" required/>
                        <button class="botao botao--assinar">Criar minha conta</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--?php $v->end(); ?-->