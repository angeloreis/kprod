<?php $v->layout("_theme"); ?>

<!-- Início da Chamada principal -->
<section class="layout-chamada">
  <div class="container">

    <div class="chamada">
      <h1 class="chamada__subtitulo">VOCÊ IMAGINA,</h1>
      <p class="chamada__titulo">NÓS REALIZAMOS!</p>
      <div class="chamada__acao">
        <!--a href="<!--?= url("/#contato"); ?>" class="botao  botao--chamada">
          Entre em contato
        </a-->
      </div>
    </div>

  </div>
</section>
<!-- Fim da Chamada principal -->

<section class="layout-servicos">
  <div class="container servicos">
    <p class="chamada__centro">Serviços</p>
    <div class="row">
      <div class="col-sm-6">
        <article class="servico__box">
          <img class="img_producao" src="<?= theme("/assets/images/producao-video.jpeg"); ?>">
          <h2>PRODUÇÃO DE VÍDEOS</h2>
          <span>
            <ul class="plano__carateristicas">
              <li>vídeos institucionais, promocionais e Perfis políticos</li>
              <li>Eventos, Documentários e Comerciais</li>
              <li>programas de TV e </li>
            </ul>
          </span>
        </article>
      </div>
      <div class="col-sm-6">
        <article class="servico__box servico__box--ultima-caixa">
          <img class="img_drone" src="<?= theme("/assets/images/drone-belem.jpeg"); ?>">
          <h2>DRONE</h2>
          <ul class="plano__carateristicas">
            <li>Equipamento registrado</li>
            <li>Alta Resolução</li>
            <li>Longo alcance</li>
          </ul>
        </article>
      </div>


    </div>
  </div>
</section>

<section id="portfolio" class="layout-portifolio">
  <div class="container servicos">
    <p class="chamada__centro">Portfólio</p>
    <div class="col-sm-12 chamada__centro">
      <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/62418632?color=6b1010&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:30px;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
      </div>
    </div>
</section>

<section id="localizacao" class="layout-localizacao servicos">
  <!--div class="container">
    <p class="chamada__centro">Localização</p>
    <div class="col-sm-12"-->
  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3988.5554319262997!2d-48.46795!3d-1.441634!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x92a48c17447fe3d1%3A0x7c1f8e3dfbe55079!2sVila%20Cardoso%2C%204498%20-%20Marco%2C%20Bel%C3%A9m%20-%20PA%2C%2066023-015!5e0!3m2!1spt-BR!2sbr!4v1594210451103!5m2!1spt-BR!2sbr" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
  <!--/div>
  </div-->
</section>

<div id="contato" class="layout-contato">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-6">
        <h4 class="titulo-formulario">Entre em contato conosco</h4>
        <div class="grupo-entrada">
          <form action="<?= url("/mensagem"); ?>" method="post" enctype="multipart/form-data">
            <div class="ajax_response"><?= flash(); ?></div>
            <?= csrf_input(); ?>
            <input type="text" name="first_name" class="campo" placeholder="Nome completo ou Razão Social:" required />
            <input type="email" name="email" class="campo" placeholder="E-mail:" required />
            <input type="text" name="subject" class="campo" placeholder="Assunto:" />
            <input type="text" name="content" class="campo" placeholder="Mensagem:" />
            <button class="botao botao--assinar menu__item--botao">Enviar</button>
          </form>
        </div>
      </div>
      <div class="col-lg-6 col-sm-6">
        <h2 class="titulo-formulario">Ou se preferir.<h4>
            <div class="contato-whatsapp">
              <h3 style="color: #fff;">Atendemos pelo WhatsApp</h3>

              <a href="https://api.whatsapp.com/send?phone=+5591982334815&text=Olá%20entrei%20no%20seu%20site.%20Gostaria%20de%20mais%20informações.Podemos%20conversar?" target="_blank">
                <img src="<?= theme('/assets/images/whatsapp.png') ?>" width="30%" alt="K Produtora - Nosso WhatsApp">
              </a>
            </div>
      </div>
    </div>
  </div>
</div>