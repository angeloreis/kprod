<?php

/**
 * DATABASE
 */
//define("CONF_DB_HOST", "localhost");
//define("CONF_DB_USER", "root");
//define("CONF_DB_PASS", "");
//define("CONF_DB_NAME", "kprod");

define("CONF_DB_HOST", "localhost");
define("CONF_DB_USER", "kprodev");
define("CONF_DB_PASS", "B!@?A2!Qv219E1Xb");
define("CONF_DB_NAME", "kprodsite");

/**
 * PROJECT URLs
 */
define("CONF_URL_BASE", "https://www.kprodutoradevideo.com.br/devsite");
define("CONF_URL_TEST", "https://www.localhost/kprodutora");
define("CONF_URL_ADMIN", "/admin");

/**
 * SITE
 */
define("CONF_SITE_NAME", "KProdutora de Vídeo");
define("CONF_SITE_TITLE", "Você imagina, Nós realizamos!");
define(
  "CONF_SITE_DESC",
  "A KProdutora de Vídeo é a empresa de multimídia que mais cresce no Estado do Pará. O seu evento registrado por nossas lentes!"
);
define("CONF_SITE_LANG", "pt_BR");
define("CONF_SITE_DOMAIN", "kprodutoradevideo.com.br");
define("CONF_SITE_ADDR_STREET", "Travessa Curuzu");
define("CONF_SITE_ADDR_NUMBER", "1498");
define("CONF_SITE_ADDR_COMPLEMENT", "Vila Cardoso , casa 15.");
define("CONF_SITE_ADDR_CITY", "Belém");
define("CONF_SITE_ADDR_STATE", "PA");
define("CONF_SITE_ADDR_ZIPCODE", "66093-801");

/**
 * SOCIAL
 */
define("CONF_SOCIAL_TWITTER_CREATOR", "@kklebersonsantos");
define("CONF_SOCIAL_TWITTER_PUBLISHER", "@kklebersonsantos");
define("CONF_SOCIAL_FACEBOOK_APP", "7635486152381752");
define("CONF_SOCIAL_FACEBOOK_PAGE", "@kklebersonsantos");
define("CONF_SOCIAL_FACEBOOK_AUTHOR", "@kklebersonsantos");
define("CONF_GOOGLE_ANALYTICS", "UA-171049474-1");
define("CONF_SOCIAL_INSTAGRAM_PAGE", "@kklebersonsantos");
define("CONF_SOCIAL_YOUTUBE_PAGE", "@kklebersonsantos");

/**
 * DATES
 */
define("CONF_DATE_BR", "d/m/Y H:i:s");
define("CONF_DATE_APP", "Y-m-d H:i:s");

/**
 * PASSWORD
 */
define("CONF_PASSWD_MIN_LEN", 8);
define("CONF_PASSWD_MAX_LEN", 40);
define("CONF_PASSWD_ALGO", PASSWORD_DEFAULT);
define("CONF_PASSWD_OPTION", ["cost" => 10]);

/**
 * VIEW
 */
define("CONF_VIEW_PATH", __DIR__ . "/../../shared/views");
define("CONF_VIEW_EXT", "php");
define("CONF_VIEW_THEME", "kprodutora");
define("CONF_VIEW_APP", "kprodadmin");

/**
 * UPLOAD
 */
define("CONF_UPLOAD_DIR", "storage");
define("CONF_UPLOAD_IMAGE_DIR", "images");
define("CONF_UPLOAD_FILE_DIR", "files");
define("CONF_UPLOAD_MEDIA_DIR", "medias");

/**
 * IMAGES
 */
define("CONF_IMAGE_CACHE", CONF_UPLOAD_DIR . "/" . CONF_UPLOAD_IMAGE_DIR . "/cache");
define("CONF_IMAGE_SIZE", 2000);
define("CONF_IMAGE_QUALITY", ["jpg" => 75, "png" => 5]);

/**
 * MAIL
 */
define("CONF_MAIL_HOST", "smtp.sendgrid.net");
define("CONF_MAIL_PORT", "587");
define("CONF_MAIL_USER", "apikey");
define("CONF_MAIL_PASS", "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");
define("CONF_MAIL_SENDER", ["name" => "Financeiro - AR Sistemas", "address" => "financeiro@arsistemas.com.br"]);
define("CONF_MAIL_SUPPORT", "angelo.desenvolvedor@gmail.com");
define("CONF_MAIL_OPTION_LANG", "br");
define("CONF_MAIL_OPTION_HTML", true);
define("CONF_MAIL_OPTION_AUTH", true);
define("CONF_MAIL_OPTION_SECURE", "tls");
define("CONF_MAIL_OPTION_CHARSET", "utf-8");
