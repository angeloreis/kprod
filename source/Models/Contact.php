<?php

namespace Source\Models;

use Source\Core\Model;

class Contact extends Model
{
    /**
     * Contact constructor.
     */
    public function __construct()
  {
    parent::__construct("contact", ["id"], ["first_name", "email", "subject", "message"]);
  }

    /**
     * @param string $firstName
     * @param string $email
     * @param string $subject
     * @param string $message
     * @return Contact
     */
    public function bootstrap(string $firstName, string $email, string $subject, string $message): Contact
  {
    $this->first_name = $firstName;
    $this->email = $email;
    $this->subject = $subject;
    $this->message = $message;
    return $this;
  }

    /**
     * @return bool
     */
    public function save(): bool
  {
    if ($this->required()) {
      $this->message->warning("Os campos Nome, email, assunto e mensagem são obrigatórios");
      return false;
    }

    if (!is_email($this->email)) {
      $this->message->warning("O e-mail informado não é válido!");
      return false;
    }

    $this->create($this->safe());
    if ($this->fail()) {
      $this->message->error("Erro ao enviar uma mensagem, tente mais tarde!");
      return false;
    }

    return true;
  }
}
