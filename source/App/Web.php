<?php

namespace Source\App;

use Source\Core\Controller;
use Source\Core\view;
use Source\Support\Email;

class Web extends Controller
{
    public function __construct()
  {
    parent::__construct(__DIR__ . "/../../themes/" . CONF_VIEW_THEME . "/");
  }

  public function home(): void
  {
    $head = $this->seo->render(
      CONF_SITE_NAME . " - " . CONF_SITE_TITLE,
      CONF_SITE_DESC,
      url(),
      theme("/assets/images/shared.png")
    );

    echo $this->view->render("home", [
      "head" => $head,
      "gAnalytcsCode" => CONF_GOOGLE_ANALYTICS,
    ]);
  }

  public function contact(array $data): void
  {
    $head = $this->seo->render(
      CONF_SITE_NAME . " - " . CONF_SITE_TITLE,
      CONF_SITE_DESC,
      url(),
      theme("/assets/images/shared.png")
    );

    echo $this->view->render("contact", [
      "head" => $head,
      "gAnalytcsCode" => CONF_GOOGLE_ANALYTICS,
    ]);
  }

  public function jobs(): void
  {
    $head = $this->seo->render(
      CONF_SITE_NAME . " - " . CONF_SITE_TITLE,
      CONF_SITE_DESC,
      url(),
      theme("/assets/images/shared.jpg")
    );

    echo $this->view->render("jobs", [
      "head" => $head,
      "gAnalytcsCode" => CONF_GOOGLE_ANALYTICS,
      "altContent" => CONF_SITE_NAME . " - " . CONF_SITE_TITLE,
    ]);
  }

  public function aboutUs(): void
  {
    $head = $this->seo->render(
      CONF_SITE_NAME . " - " . CONF_SITE_TITLE,
      CONF_SITE_DESC,
      url(),
      theme("/assets/images/shared.png")
    );

    echo $this->view->render("aboutUs", [
      "head" => $head,
      "gAnalytcsCode" => CONF_GOOGLE_ANALYTICS,
    ]);
  }

  public function mensagem(array $data): void
  {
      if (!empty($data['csrf'])) {
          if (!csrf_verify($data)) {
              $json['message'] = $this->message->error("erro ao enviar, favor use o formulário!")->render();
              echo json_encode($json);
              return;
          }

          if (in_array("",$data)) {
              $json['message'] = $this->message->info("Informe dados para poder prosseguir com a operação!")->render();
              echo json_encode($json);
              return;
          }

          if (request_limit("webmensagem", 5, 60 * 5)) {
              $json["message"] = $this->message->warning("Por favor, aguarde 5 minutos! Não faça spam!")->render();
              echo json_encode($json);
              return;
          }

          if (request_repeat("content", $data["content"])) {
              $json["message"] = $this->message->info("Já recebemos usa solicitação {$data["first_name"]}. Agradecemos o contato, iremos responder em breve!")->render();
              echo json_encode($json);
              return;
          }

          $subject = date_fmt() . " - {$data["subject"]}";
          $message = filter_var($data["content"], FILTER_SANITIZE_STRING);

          $view = new View(__DIR__ . "/../../shared/views/email");
          $body = $view->render("mail", [
              "subject" => $subject,
              "message" => str_textarea($message)
          ]);

          $email = new Email();

          $email->bootstrap(
              $subject,
              $body,
              CONF_MAIL_SUPPORT,
              "Suporte via " . CONF_SITE_NAME
          //)->queue($data["email"], "{$data["first_name"]} <{$data["email"]}>");
          )->queue($data["email"], "{$data["first_name"]}");

          $this->message->success("Recebemos sua solicitação {$data['first_name']}! Agradecemos muito o contato, iremos responder em breve!")->render();
          $json["reload"] = true;
          echo json_encode($json);
          return;
      }
  }

  public function error(array $data): void
  {

    var_dump($data);

    $error = new \stdClass();

    switch ($data['errcode']) {
      case "problemas":
        $error->code = "OPS";
        $error->title = "Estamos enfrentando problemas!";
        $error->message = "Parece que nosso serviço não está diponível no momento. Já estamos vendo isso mas caso precise, envie um e-mail :)";
        $error->linkTitle = "ENVIAR E-MAIL";
        $error->link = "mailto:" . CONF_MAIL_SUPPORT;
        break;

      case "manutencao":
        $error->code = "OPS";
        $error->title = "Desculpe. Estamos em manutenção!";
        $error->message = "Voltamos logo! Por hora estamos trabalhando para melhorar nosso conteúdo para você controlar melhor as suas contas :P";
        $error->linkTitle = null;
        $error->link = null;
        break;

      default:
        $error->code = $data['errcode'];
        $error->title = "Ooops. Conteúdo indispinível :/";
        $error->message = "Sentimos muito, mas o conteúdo que você tentou acessar não existe, está indisponível no momento ou foi removido :/";
        $error->linkTitle = "Continue navegando!";
        $error->link = url_back();
        break;
    }

    $head = $this->seo->render(
      "{$error->code} | {$error->title}",
      $error->message,
      url("/ops/{$error->code}"),
      theme("/assets/images/share.png"),
      false
    );

    echo $this->view->render("error", [
      "head" => $head,
      "error" => $error,
      "gAnalytcsCode" => CONF_GOOGLE_ANALYTICS,
    ]);
  }
}
